

import org.json.JSONObject;
import org.lightcouch.CouchDbClient;
import org.lightcouch.Response;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) throws IOException {

        String webSite = "http://vir.inf.u-szeged.hu:8090/getToken?nid=XYZ";
        HttpURLConnection conn = createConnection(webSite, "POST");
        ArrayList<String> finalValues = storeResponseValuesInList(conn);

        CouchDbClient dbClient = new CouchDbClient("vir_db", true,
                "http", "vir.inf.u-szeged.hu", 5986,
                "VIR", "VIRPass123");

        NeptunUser neptunUser = new NeptunUser();
        neptunUser.setId(Integer.parseInt(finalValues.get(0)));
        neptunUser.setNid(finalValues.get(1));
        neptunUser.setToken(finalValues.get(2));

        Response response = dbClient.save(neptunUser);
        System.out.println(response);
    }

    private static HttpURLConnection createConnection (String webSite, String type) throws IOException {
        URL url = new URL(webSite);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setDoOutput(true);
        conn.setRequestMethod(type);
        conn.setRequestProperty("Content-Type", "application/json; utf-8");
        return conn;
    }

    private static String responseOutput (HttpURLConnection conn) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String inputLine = "";
        StringBuilder responseInStringFormat = new StringBuilder();
        while ((inputLine = bufferedReader.readLine()) != null) {
            responseInStringFormat.append(inputLine);
        }
        return responseInStringFormat.toString();
    }

    private static ArrayList<String> storeResponseValuesInList (HttpURLConnection conn) throws IOException {
        String finalJSONFormat = responseOutput(conn);
        JSONObject jsonObject = new JSONObject(finalJSONFormat);

        //lekerjuk a json response-ban szereplo ertekeket
        //pl.: { "id": "123", "nid": "123456", "token": "99999999999" }

        String finalId = (String) jsonObject.get("id"); //"123
        String finalNid = (String) jsonObject.get("nid"); //123456
        String finalToken = (String) jsonObject.get("token"); //99999999999

        ArrayList<String> jsonResponseValues = new ArrayList<String>();
        jsonResponseValues.add(finalId);
        jsonResponseValues.add(finalNid);
        jsonResponseValues.add(finalToken);

        return jsonResponseValues;
    }


}
